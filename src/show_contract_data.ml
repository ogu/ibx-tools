open Core
open Async
open Ibx

let () =
  Command.async_or_error
    ~summary:"Show detailed contract information of stocks, futures and indices"
    Command.Spec.(
      Common.common_args ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> Common.sec_type_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id currency exchange sec_type symbol () ->
       Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
         Common.create_contract tws ~currency ~exchange ~sec_type ~symbol
         >>= fun contract ->
         Tws.contract_data_exn tws ~contract
         >>= fun data ->
         print_endline (Contract_data.sexp_of_t data |> Sexp.to_string_hum);
         return ()
       )
    )
  |> Command.run
