open Core
open Async
open Ibx

type position_data = {
  contract_name : string;
  size : Volume.t;
  currency : Currency.t;
  market_price : Price.t;
  market_value : Price.t;
  total_pnl : Price.t;
  return : float;
  days_to_expiry : int option;
  underlying : Price.t option;
}

let show_portfolio positions =
  Textutils.Ascii_table.(output ~oc:stdout ~limit_width_to:130 [
    Column.create "Contract" ~align:Align.Left
      (fun pos -> sprintf "%s" pos.contract_name);
    Column.create "Position" ~align:Align.Right
      (fun pos -> sprintf "%d" (pos.size :> int));
    Column.create "Currency" ~align:Align.Center
      (fun pos -> sprintf "%s" (Currency.to_string pos.currency));
    Column.create "Price" ~align:Align.Right
      (fun pos -> sprintf "%4.2f" (pos.market_price :> float));
    Column.create "Value" ~align:Align.Right
      (fun pos -> sprintf "%4.2f" (pos.market_value :> float));
    Column.create "P&L" ~align:Align.Right
      (fun pos -> sprintf "%4.2f" (pos.total_pnl :> float));
    Column.create "Return" ~align:Align.Right
      (fun pos -> sprintf "%2.2f %%" (pos.return *. 100.));
    Column.create "DTE" ~align:Align.Right
      (fun pos -> sprintf "%s"
          (Option.value_map pos.days_to_expiry ~default:"-" ~f:Int.to_string));
    Column.create "Underlying" ~align:Align.Right
      (fun pos -> sprintf "%s"
          (Option.value_map pos.underlying ~default:"-"
             ~f:(fun price -> sprintf "%4.2f" (Price.to_float price))));
  ] positions)

type non_derivative = [ `Stock | `Index | `Forex ]

let () =
  Command.async_or_error
    ~summary:"Show all portfolio positions"
    Command.Spec.(Common.common_args ())
    (fun do_logging host port client_id () ->
       Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
         let last_price contract =
           Tws.latest_trade tws ~contract >>= function
           | Ok trade ->
             return (Some (Trade.price trade))
           | Error _ -> (* Use closing price as proxy. *)
             Tws.latest_close tws ~contract >>| function
             | Ok close ->
               Some (Close.price close)
             | Error e ->
               prerr_endline (Error.to_string_hum e); None
         in
         let days_to_expiry contract =
           Tws.contract_data tws ~contract >>| function
           | Error e ->
             prerr_endline (Error.to_string_hum e); None
           | Ok data ->
             Option.map (Contract_data.time_zone data) ~f:(fun zone ->
               Contract.days_to_expiry contract ~zone
             )
         in
         let%bind positions = Tws.portfolio_exn tws in
         Pipe.map' positions ~f:(fun position_q ->
           Deferred.Queue.map position_q ~how:`Parallel ~f:(fun position ->
             let contract = Position.contract position in
             let contract_name = Contract.to_string contract in
             (match Contract.sec_type contract with
              | `Option | `Fut_opt ->
                let underlying = Contract.underlying contract in
                Deferred.both (last_price underlying) (days_to_expiry contract)
              | `Futures ->
                let%bind dte = days_to_expiry contract in
                return (None, dte)
              | #non_derivative ->
                return (None, None)
             ) >>| fun (under_price, days_to_expiry) ->
             { contract_name;
               size = Position.size position;
               currency = Contract.currency contract;
               market_price = Position.market_price position;
               market_value = Position.market_value position;
               total_pnl = Position.total_pnl position;
               return = Position.return position;
               days_to_expiry = days_to_expiry;
               underlying = under_price;
             }
           )
         ) |> Pipe.to_list >>| show_portfolio
       )
    ) |> Command.run
