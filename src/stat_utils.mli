(** Utilities for statistical computations. *)

(** [quickselect arr n] finds the [n]-th smallest element in the array [arr]. *)
val quickselect_inplace : float array -> int -> float
