open Core

(* Implementation is based on http://en.wikipedia.org/wiki/Quickselect *)
let quickselect_inplace arr n =
  let partition left right pivot_index =
    let pivot_value = arr.(pivot_index) in
    Array.swap arr pivot_index right;  (* Move pivot to end *)
    let store_index = ref left in
    for i = left to right-1 do
      if arr.(i) < pivot_value then begin
        Array.swap arr !store_index i;
        incr store_index
      end;
    done;
    Array.swap arr right !store_index;  (* Move pivot to its final place *)
    !store_index
  in
  let rec select left right =
    assert (left <= n && n <= right);
    if left = right then
      arr.(left)  (* Return the single element in the array. *)
    else
      let pivot_index = (left + right) / 2 in
      let pivot_index = partition left right pivot_index in
      (* The pivot is in its final sorted position. *)
      if n = pivot_index then
        arr.(n)
      else if n < pivot_index then
        select left (pivot_index-1)
      else
        select (pivot_index+1) right
  in
  select 0 (Array.length arr-1)
