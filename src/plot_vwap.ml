open Core
open Async
open Ibx
open Gnuplot

let () =
  Command.async_or_error
    ~summary:"Plot the VWAP of the given symbol"
    Command.Spec.(
      Common.common_args ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> Common.sec_type_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id currency exchange sec_type symbol () ->
       Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
         Common.create_contract tws ~currency ~exchange ~sec_type ~symbol
         >>= fun contract ->
         Tws.contract_data_exn tws ~contract
         >>= fun contract_data ->
         let zone =
           Contract_data.time_zone contract_data
           |> Option.value ~default:(Lazy.force Time.Zone.local)
         in
         Tws.history_exn tws ~contract ~duration:(`Day 1) ~bar_size:`One_min
         >>| fun history ->
         let start = Time.sub (History.start history) Time.Span.minute in
         let stop = Time.add (History.stop history) Time.Span.minute in
         let time_ohlc = History.time_ohlc history in
         let time_vwap = History.time_vwap history in
         let date = Time.to_date stop ~zone in
         let name = Contract.to_string contract in
         let gp = Gp.create () in
         Gp.plot_many gp ~range:(Range.Time (start, stop, zone)) ~use_grid:true
           ~title:(sprintf "VWAP of %s (%s)" name (Date.to_string date)) [
           (* Create a candlestick chart series. *)
           Series.candles_time_ohlc time_ohlc ~zone ~title:"1 min bars" ~color:`Red;
           (* Create a VWAP time series. *)
           Series.lines_timey time_vwap ~zone ~title:"VWAP" ~color:`Blue;
         ];
         Gp.close gp
       )
    )
  |> Command.run
