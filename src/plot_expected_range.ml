open Core
open Async
open Ibx
open Gnuplot

let () =
  Command.async_or_error
    ~summary:"Plot the expected range induced by implied volatility"
    Command.Spec.(
      Common.common_args ()
      +> flag "-days" (optional_with_default 1 int)
        ~doc:" time horizon of expected range computation"
      +> flag "-n-std-dev" (optional_with_default 1 int)
        ~doc:" number of standard deviations"
      +> Common.duration_arg ()
      +> Common.bar_size_arg ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> Common.sec_type_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id days n_std_dev duration bar_size
      currency exchange sec_type symbol () ->
      Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
        Common.create_contract tws ~currency ~exchange ~sec_type ~symbol
        >>= fun contract ->
        Tws.contract_data_exn tws ~contract
        >>= fun contract_data ->
        let zone =
          Contract_data.time_zone contract_data
          |> Option.value ~default:(Lazy.force Time.Zone.local)
        in
        Deferred.both
          (Tws.history_exn tws ~contract ~duration ~bar_size)
          (Tws.history_exn tws ~contract ~duration:(`Day 1)
             ~bar_size:`One_day ~tick_type:`Implied_volatility)
        >>| fun (px_history, iv_history) ->
        let start = Time.sub (History.start px_history) Time.Span.day in
        let stop = Time.add (History.stop px_history) Time.Span.day in
        let last_close h =
          History.time_ohlc h |> List.last_exn |> fun (_, (_,_,_,c)) -> c
        in
        let s0 = last_close px_history in
        let sigma = last_close iv_history in
        let duration = Float.(of_int days / 252.) in
        let n_std_dev = Float.of_int n_std_dev in
        let move = Float.(s0 * (exp (n_std_dev * sigma * sqrt duration) - 1.)) in
        printf "Expected Range = [%.2f, %.2f]\n" (s0 -. move) (s0 +. move);
        (* Plot the candles and the expected range induced by implied vol. *)
        let hline ?title ?color y =
          Series.steps_timey ?title ?color ~zone [start, y; stop, y]
        in
        let time_ohlc = History.time_ohlc px_history in
        let name = Common.escape (Contract.to_string contract) in
        let gp = Gp.create () in
        Gp.plot_many gp
          ~title:(sprintf "Expected Range for %s" name) ~use_grid:true
          ~format:"%b %d'%y" ~range:(Range.Time (start, stop, zone))
          [ Series.candles_time_ohlc time_ohlc ~zone ~title:"Price"
          ; hline (s0 -. move) ~color:`Blue ~title:"Up Move"
          ; hline (s0 +. move) ~color:`Blue ~title:"Down Move"];
        Gp.close gp
      )
    )
  |> Command.run
