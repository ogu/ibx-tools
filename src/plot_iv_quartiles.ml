open Core
open Async
open Ibx
open Gnuplot

type quartiles = {
  lowerq : float;
  upperq : float;
  median : float;
}

let calc_quartiles_inplace xs =
  let n = Array.length xs in
  { lowerq = Stat_utils.quickselect_inplace xs (n / 4);
    upperq = Stat_utils.quickselect_inplace xs (3 * n / 4);
    median = Stat_utils.quickselect_inplace xs (n / 2) }
;;

let () =
  Command.async_or_error ~summary:"Plot IV quartiles"
    Command.Spec.(
      Common.common_args ()
      +> Common.duration_arg ()
      +> Common.bar_size_arg ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> Common.sec_type_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id duration bar_size
      currency exchange sec_type symbol () ->
      Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
        Common.create_contract tws ~currency ~exchange ~sec_type ~symbol
        >>= fun contract ->
        Tws.contract_data_exn tws ~contract
        >>= fun contract_data ->
        let zone =
          Contract_data.time_zone contract_data
          |> Option.value ~default:(Lazy.force Time.Zone.local)
        in
        Tws.history_exn tws ~contract ~duration ~bar_size
          ~tick_type:`Implied_volatility
        >>| fun history ->
        let ivs = History.(unpack_bars history |> Data_frame.cl) in
        (* Print 30-day implied volatility based on at-the-money option prices
           from two consecutive expiration months.  *)
        printf "IV = %g\n" (Array.last ivs);
        let qs = calc_quartiles_inplace ivs in
        (* Print the IV quartiles on the console. *)
        printf "IV lower quartile = %g\n" qs.lowerq;
        printf "IV upper quartile = %g\n" qs.upperq;
        printf "IV median = %g\n" qs.median;
        (* Plot the IV time series and its quartiles. *)
        let start, stop = History.(start history, stop history) in
        let time_iv h =
          History.time_ohlc h |> List.map ~f:(fun (t, (_,_,_,c)) -> t, c)
        in
        let hline ?title ?color y =
          Series.steps_timey ?title ?color ~zone [start, y; stop, y]
        in
        let gp = Gp.create () in
        Gp.plot_many gp
          ~title:(Common.escape (Contract.to_string contract)) ~use_grid:true
          ~format:"%b %d'%y" ~range:(Range.Time (start, stop, zone))
          [ Series.lines_timey (time_iv history )~zone ~color:`Green ~title:"IV"
          ; hline qs.upperq ~color:`Blue ~title:"IV upper quartile"
          ; hline qs.median ~color:`Red  ~title:"IV median"
          ; hline qs.lowerq ~color:`Blue ~title:"IV lower quartile" ];
        Gp.close gp
      )
    )
  |> Command.run
