open Core
open Async
open Ibx

let escape =
  let module E = String.Escaping in
  Staged.unstage (E.escape ~escapeworthy:['\''; '_'; '&'] ~escape_char:'\\')
;;

module Client_id = struct
  let default = Client_id.of_int_exn 0
  let arg_type = Command.Spec.Arg_type.create Client_id.of_string
end

let common_args () =
  Command.Spec.(
    empty
    +> flag "-log" no_arg
      ~doc:" enable logging"
    +> flag "-host" (optional_with_default "127.0.0.1" string)
      ~doc:" hostname of TWS or Gateway (default localhost)"
    +> flag "-port" (optional_with_default 4001 int)
      ~doc:" TWS port 7496 or Gateway port 4001 (default 4001)"
    +> flag "-client-id" (optional_with_default Client_id.default Client_id.arg_type)
      ~doc:" client id of TWS or Gateway (default 0)"
  )

module Duration = struct
  let arg_type = Command.Spec.Arg_type.create Bar.Duration.of_string
end

let duration_arg () =
  Command.Spec.(
    flag "-span" (optional_with_default (`Year 1) Duration.arg_type)
      ~doc:" the time covered by the historical data request"
  )

module Bar_size = struct
  let arg_type = Command.Spec.Arg_type.create Bar.Size.of_string
end

let bar_size_arg () =
  Command.Spec.(
    flag "-size" (optional_with_default `One_day Bar_size.arg_type)
      ~doc:" the size of the bars that will be returned"
  )

let sma_period_arg () =
  Command.Spec.(
    flag "-sma" (optional int)
      ~doc:" the look-back period of the simple moving average"
  )

module Currency = struct
  let arg_type = Command.Spec.Arg_type.create Currency.of_string
end

let currency_arg () =
  Command.Spec.(
    flag "-currency" (optional_with_default `USD Currency.arg_type)
      ~doc:" the currency of the symbol"
  )

module Exchange = struct
  let arg_type = Command.Spec.Arg_type.create Exchange.of_string
end

let exchange_arg () =
  Command.Spec.(
    flag "-exchange" (optional_with_default `SMART Exchange.arg_type)
      ~doc:" the exchange of the symbol"
  )

module Sec_type = struct
  module T = struct
    type t = [ `Stock | `Index | `Futures ] [@@deriving sexp]
  end
  include Sexpable.To_stringable (T)
  let arg_type = Command.Spec.Arg_type.create of_string
end

let create_contract tws ~currency ~exchange ~sec_type ~symbol =
  match sec_type with
  | `Futures ->
    let%bind chain = Tws.futures_chain_exn tws symbol ~currency ~exchange in
    let%bind front_month =
      Pipe.to_list chain
      >>| Contract.sort_futures_chain
      >>| List.hd_exn
    in
    return front_month
  | `Stock ->
    return (Contract.stock symbol ~currency ~prim_exch:exchange)
  | `Index ->
    return (Contract.index symbol ~currency ~exchange)

let sec_type_arg () =
  Command.Spec.(
    flag "-sec-type" ~doc:" the security type of the symbol"
      (optional_with_default `Stock Sec_type.arg_type)
  )
