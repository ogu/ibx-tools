open Core
open Async
open Ibx
open Gnuplot

let () =
  Command.async_or_error ~summary:"Plot E-Mini trades"
    Command.Spec.(Common.common_args ())
    (fun do_logging host port client_id () ->
       Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
         let trades contract ~action =
           Tws.filter_executions_exn tws ~contract ~action
           >>= fun exec_pipe ->
           Pipe.to_list exec_pipe
           >>| fun exec_list ->
           Option.some_if (not (List.is_empty exec_list))
             (List.map exec_list ~f:(fun exec ->
                Execution.(time exec, (price exec :> float))))
         in
         let es = Symbol.of_string "ES" in
         let%bind chain = Tws.futures_chain_exn tws es ~exchange:`GLOBEX ~currency:`USD in
         let%bind sorted_chain = Pipe.to_list chain >>| Contract.sort_futures_chain in
         let front_month = List.hd_exn sorted_chain in
         Deferred.both (trades front_month ~action:`Buy) (trades front_month ~action:`Sell)
         >>= fun (buys, sells) ->
         Tws.history_exn tws ~contract:front_month ~duration:(`Day 1) ~bar_size:`Five_min
         >>| fun history ->
         let start = Time.sub (History.start history) Time.Span.minute in
         let stop = Time.add (History.stop history) Time.Span.minute in
         let zone = Time.Zone.find_exn "America/Chicago" in
         let range = Range.Time (start, stop, zone) in
         let date = Date.of_time stop ~zone in
         let name = Option.value_exn (Contract.local_symbol front_month) in
         let gp = Gp.create () in
         Gp.set gp ~title:(sprintf "E-Mini Trades on %s" (Date.to_string date));
         [ (* Create a candlestick chart series. *)
           Series.candles_time_ohlc ~zone ~color:`Blue
             ~title:(sprintf "5-minute bars of %s" (name :> string))
             (History.time_ohlc history) |> Option.some;
           (* Create point series of all the trades. *)
           Option.map buys ~f:(fun buys ->
             Series.points_timey buys ~zone ~title:"Bought" ~color:`Green);
           Option.map sells ~f:(fun sells ->
             Series.points_timey sells ~zone ~title:"Sold" ~color:`Red);
         ] |> List.filter_opt |> Gp.plot_many gp ~use_grid:true ~range;
         Gp.close gp
       )
    )
  |> Command.run
