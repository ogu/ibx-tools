open Core
open Async
open Ibx
open Gnuplot

module Filter = struct
  let sma ~period =
    if period <= 0 then
      invalid_argf "Filter.sma: period must be positive: %d" period ();
    let q = Queue.create () in
    let sum = ref 0.0 in
    stage (fun x_new ->
      sum := !sum +. x_new;
      Queue.enqueue q x_new;
      if Queue.length q > period then begin
        let x_last = Queue.dequeue_exn q in
        sum := !sum -. x_last
      end;
      !sum /. float (Queue.length q))
end

let () =
  Command.async_or_error
    ~summary:"Show a candlestick chart of historical prices"
    Command.Spec.(
      Common.common_args ()
      +> Common.duration_arg ()
      +> Common.bar_size_arg ()
      +> Common.sma_period_arg ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> Common.sec_type_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id duration bar_size period
      currency exchange sec_type symbol () ->
      Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
        Common.create_contract tws ~currency ~exchange ~sec_type ~symbol
        >>= fun contract ->
        Tws.contract_data_exn tws ~contract
        >>= fun contract_data ->
        let zone =
          Contract_data.time_zone contract_data
          |> Option.value ~default:(Lazy.force Time.Zone.local)
        in
        Tws.history_exn tws ~duration ~bar_size ~contract
        >>| fun history ->
        let start = Time.sub (History.start history) Time.Span.day in
        let stop = Time.add (History.stop history) Time.Span.day in
        let name = Contract.to_string contract in
        let gp = Gp.create () in
        Gp.set gp ~title:(Common.escape (sprintf "%s" name)) ~use_grid:true;
        [ (* Create a candlestick chart series. *)
          Series.candles_time_ohlc (History.time_ohlc history)
            ~zone ~title:"Price" ~color:`Red |> Option.some
          (* Create a moving average time series of the closing prices. *)
        ; Option.map period ~f:(fun period ->
            let sma = unstage (Filter.sma ~period) in
            Series.lines_timey ~zone ~color:`Green ~title:(sprintf "SMA %d" period)
              (List.map (History.bars history) ~f:(fun bar ->
                 Bar.(stamp bar, sma (cl bar :> float)))));
        ] |> List.filter_opt |> Gp.plot_many gp ~range:(Range.Time (start, stop, zone));
        Gp.close gp
      )
    )
  |> Command.run
