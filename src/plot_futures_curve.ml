open Core
open Async
open Ibx
open Gnuplot

let () =
  Command.async_or_error
    ~summary:"Plot the futures curve of the given contract"
    Command.Spec.(
      Common.common_args ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id currency exchange symbol () ->
       Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
         let%bind chain = Tws.futures_chain_exn tws ~currency ~exchange symbol in
         let%bind sorted_chain = Pipe.to_list chain >>| Contract.sort_futures_chain in
         let front_month = List.hd_exn sorted_chain in
         let%bind contract_data = Tws.contract_data_exn tws ~contract:front_month in
         let throttle = Throttle.create
             ~continue_on_error:false ~max_concurrent_jobs:10 in
         Deferred.List.map sorted_chain ~how:`Parallel ~f:(fun futures ->
           Throttle.enqueue throttle (fun () ->
             let%map close = Tws.latest_close_exn tws ~contract:futures in
             Contract.expiry futures, (Close.price close :> float))
         ) >>| fun prices ->
         let start = List.hd_exn prices |> Tuple2.get1 in
         let stop = List.last_exn prices |> Tuple2.get1 in
         let gp = Gp.create () in
         let title = sprintf "%s Futures Curve (%s - %s)"
             (Contract_data.long_name contract_data |> Common.escape)
             (Date.to_string_american start) (Date.to_string_american stop)
         in
         Gp.plot gp ~title ~range:(Range.Date (start, stop))
           ~format:"%b %d'%y" ~use_grid:true (
           Series.linespoints_datey prices ~color:`Green ~title:"Close"
         );
         Gp.close gp;
       )
    )
  |> Command.run
