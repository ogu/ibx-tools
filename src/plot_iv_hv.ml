open Core
open Async
open Ibx
open Gnuplot

let () =
  Command.async_or_error ~summary:"Plot 30-day IV vs HV"
    Command.Spec.(
      Common.common_args ()
      +> Common.duration_arg ()
      +> Common.bar_size_arg ()
      +> Common.currency_arg ()
      +> Common.exchange_arg ()
      +> Common.sec_type_arg ()
      +> anon ("SYMBOL" %: Arg_type.create Symbol.of_string)
    )
    (fun do_logging host port client_id duration bar_size
      currency exchange sec_type symbol () ->
      Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
        Common.create_contract tws ~currency ~exchange ~sec_type ~symbol
        >>= fun contract ->
        Tws.contract_data_exn tws ~contract
        >>= fun contract_data ->
        let zone =
          Contract_data.time_zone contract_data
          |> Option.value ~default:(Lazy.force Time.Zone.local)
        in
        Deferred.both
          (Tws.history_exn tws ~contract ~duration ~bar_size
             ~tick_type:`Implied_volatility)
          (Tws.history_exn tws ~contract ~duration ~bar_size
             ~tick_type:`Historical_volatility)
        >>| fun (iv_history, hv_history) ->
        let start, stop = History.(start iv_history, stop iv_history) in
        let time_series h =
          History.time_ohlc h |> List.map ~f:(fun (t, (_,_,_,c)) -> t, c)
        in
        let gp = Gp.create () in
        Gp.plot_many gp ~title:(Common.escape (Contract.to_string contract))
          ~format:"%b %d'%y" ~range:(Range.Time (start, stop, zone)) ~use_grid:true
          [ Series.lines_timey (time_series iv_history) ~zone
              ~title:"30-day implied volatility" ~color:`Green
          ; Series.lines_timey (time_series hv_history) ~zone
              ~title:"30-day historical volatility" ~color:`Blue ];
        Gp.close gp
      )
    )
  |> Command.run
