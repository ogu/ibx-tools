open Core
open Async
open Ibx

module Csv_writer = Core_extended.Csv_writer

let csv_header =
  [ "contract"
  ; "exchange"
  ; "position"
  ; "currency"
  ; "marketPrice"
  ; "marketValue"
  ; "averageCost"
  ; "realizedPNL"
  ; "unrealizedPNL"
  ] |> Csv_writer.line_to_string
;;

let position_to_csv position =
  let module P = Position in
  [ sprintf "%s" (P.contract position |> Contract.to_string)
  ; sprintf "%s" (P.contract position |> Contract.exchange |> Exchange.to_string)
  ; sprintf "%d" (P.size position :> int)
  ; sprintf "%s" (P.contract position |> Contract.currency |> Currency.to_string)
  ; sprintf "%4.2f" (P.market_price position :> float)
  ; sprintf "%4.2f" (P.market_value position :> float)
  ; sprintf "%4.2f" (P.average_cost position :> float)
  ; sprintf "%4.2f" (P.realized_pnl position :> float)
  ; sprintf "%4.2f" (P.unrealized_pnl position :> float)
  ] |> Csv_writer.line_to_string
;;

let () =
  Command.async_or_error ~summary:"Save portfolio to CSV file"
    Command.Spec.(Common.common_args ())
    (fun do_logging host port client_id () ->
       Tws.with_client_or_error ~do_logging ~host ~port ~client_id (fun tws ->
         let account_code = Option.value_exn (Tws.account_code tws) in
         let filename = sprintf "%s_portfolio.csv" (account_code :> string) in
         Writer.with_file filename ~f:(fun writer ->
           Writer.write_line writer csv_header;
           Tws.portfolio_exn tws >>= fun positions ->
           Pipe.iter_without_pushback positions ~f:(fun position ->
             Writer.write_line writer (position_to_csv position))
         )
       )
    )
  |> Command.run
